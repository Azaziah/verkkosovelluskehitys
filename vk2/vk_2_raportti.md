### Olli Suutari | Verkkosovelluskehitys vk 2

### Viikon 2 raportti


#### JSON kirjautuminen

Tutustuminen JSONIIN oli mielenkiintoista.
Loppupeleissä tarvittavat muutokset työstämääni vk 2 kompponenttisovellukseen olivat melko pieniä.
Rupesin kuitenkin kikkailemaan vähän turhankin paljon ja tähän meni jonkin verran aikaa.

Toki tätä olisi voinut toteuttaa vielä lisääkin.

Sovelluksen käynnistys:
docker build -t docker/mongoserver:1.0 .
docker run -p 27017:27017 -it docker/mongoserver:1.0

[Koodi löytyy tämän linkin takaa](https://gitlab.com/Azaziah/verkkosovelluskehitys/tree/master/vk2/JSON-kirjautuminen)


#### Simple chat server

Tutustuminen Telnettiin oli mukavaa, sain sovelluksen toimimaan useailla yhteyksillä sekä nimen vaihdolla. Tehtävä osoittautui paikoitellen yllättävän hankalaksi ja ajanpuuteen vuoksi se jäi osittain tekemättä.

Koodi:
```
'use strict';
const net = require('net');

// Komentojen oikeellisuden tarkistus Regexin avulla.
const check_nick = new RegExp("^NICK\\s\\S+");
const check_who = new RegExp("^WHOIS\\s\\S+");
const check_msg = new RegExp("^MSG\\s\\S+\\s\\S+");

// Lista käyttäjistä
let clients = [];
let quest_number = 1;

// Palvelimen alustus.
net.createServer(function (socket) {
    // Enkoodaus.
    socket.setEncoding('utf8');
    // Asetetaan nimi.
    socket.name = "Vieras_" + quest_number;
    quest_number++;
    clients.push(socket);
    // Tervetuloa + tieto muille.
    socket.write("Tervetuloa " + socket.name + "\n");
    broadcast(socket.name + " liittyi chättiin\n", socket);

    // Viestien käsittely.
    socket.on('data', function(buffer) {
        // Parsitaan turhat merkit pois.
        const message = buffer.trim(/(r\n|\n|\r)/gm,"");
        // Käydään läpi mahdolliset komennot.
        if(message === "HELP")
        {
          console.log("NICK - nimen vaihto\n" +
              "MSG nimi - yksityisviesti\n" +
              "WHOIS nimi - henkilön tiedot");
        }
        // Tarkistetaan, että syntaksi on oikein.
        else if (check_nick.test(message))
        {
            socket.name = message.substr(message.indexOf(' ')+1);
            console.log("Nimen vaihto onnistui.");
        }
        else if (check_msg.test(message))
        {
            // Pilkkominen toisen välilyönnin jälkeen.
            const index = message.indexOf( ' ', message.indexOf( ' ' ) + 1 );
            const before_message = message.substr( 0, index );
            // Haetaan vastaanottaja.
            const to_message = before_message.replace("MSG ", "");
            // Haetaan viesti (kaikki vastaanottajan jälkeen.)
            const message_to_send = message.substr( index + 1 );
            // TO DO: Funktio viesin lähettämiseen clients käyttäjälle X.
            console.log("Yksityisviesti käyttäjälle: " + to_message + "\n VIESTI: " + message_to_send);
        }
        else if (check_who.test(message))
        {
            // TO DO: Lisää käyttäjän tietojen tulostusfunktio.
            console.log("Käyttäjän tiedot...");
        }
        // Jos ei komento. Poistetaan rivinvaihtojen tulostukset. Vaaditaan yhdessä trimmin kanssa.
        else if(message.length > 0)
        {
            broadcast(socket.name + ": " + message, socket);
        }
  });
  // Poistetaan käyttäjä listasta yhteyden sulkeutuessa.
  socket.on('end', function () {
    clients.splice(clients.indexOf(socket), 1);
    broadcast(socket.name + " poistui chatistä.\n");
  });

  // Lähettää viestin kaikille käyttäjille.
  function broadcast(message, sender) {
    clients.forEach(function (client) {
      // Ei lähetetä viestin lähettäjälle.
      if (client === sender) return;
      client.write(message + "\n");
    });
    // Log it to the server output too
    process.stdout.write(message + "\n")
  }

}).listen(8000);
console.log("Chätti päällä portissa: 8000\n");
```