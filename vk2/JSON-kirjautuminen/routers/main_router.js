'use strict';

const express = require('express');
const router = express.Router();
const path = require('path');
const fs = require('fs');

const postman = require('postman');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../schemas/user_schema.js');
const Student = require('../schemas/student_schema.js');

router.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'login.html'));
});

router.post('/register', (req, res) => {
    User.find({'username': req.body.username})
        .then((result) => {
            if(result.length !== 0)
                throw new Error('Tällä sähköpostilla on jo luotu tunnus.');
            if(req.body.username === "")
                throw new Error("Käyttäjänimi vaaditaan.");
            if(req.body.password === "")
                throw new Error("Salasana vaaditaan.");
            else {
                //const hashed_password = bcrypt.hashSync(req.body.password, 10);
                let user = new User({
                    "username": req.body.username,
                    "password": req.body.password,
                });
                console.log('Lisättiin käyttäjä ' + req.body.username);
                res.end();
                return user.save();
            }
        })
        .catch((reason) => {
            console.log("Tapahtui virhe: " + reason);
            res.status(500).send('Something broke! ' + reason)
        });
});

router.post('/login', (req, res) => {

    User.find({'username': req.body.username})
        .then((result) => {
            if (result.length === 0)
                throw new Error('Tiliä ei löydetty tällä käyttäjänimellä.');
            else if(result[0].toObject().password !== req.body.password)
            {
                throw new Error('Salasana oli virheellinen.');
            }
            else
            {
                // Tämän perusteella vosi lukita muita funktiota niin, että ne vaativat sisäänkirjautumisen.
                console.log("Login onnistui! " + result[0].toObject().username);
                return res.json({token: jwt.sign({ username: result[0].toObject().username, _id: result[0].toObject()._id}, 'RESTFULAPIs')});
            }
        })
});


// Uuden opiskelijan lisäys.
router.put('/addStudent', (req, res) => {
        const studentData = req.body.studentData;
        let item = new Student({
            name: studentData.name,
            surname: studentData.surname,
            callingName: studentData.callingName,
            studentId: studentData.studentId,
            email: studentData.email,
        });
        console.log('Lisättiin opiskelija ' + studentData.name + ' ' + studentData.surname);
        res.end();
        return item.save();
    });

router.get('/getAllStudents', function(req, res) {

    let students = [];
    let promises = [];

    Student.find({})
        .then((usersFound) => {
            students = usersFound;
            for (let i = 0; i < students.length; i++) {
                promises.push(students[i]);
            }
            return Promise.all(promises);
        })
        .then((students) => {
            res.send({
                students: students,
            });
        })
        .catch((reason) => {
            res.send({
                students: null,
                error: reason
            });
        });
});

router.post('/getSearchedStudents', function(req, res) {
    let students = [];
    let promises = [];
    const surname_to_find = req.body.surname;
    const student_id_to_find = req.body.studentId;
    if(surname_to_find != null && student_id_to_find == null)
    {
        console.log("Haku sukunimellä...");
        // Hakee hakutermit sisältäviä nimiä.
        Student.find({"surname":{'$regex' : surname_to_find, '$options' : 'i'}})
            .then((usersFound) => {
                students = usersFound;
                for (let i = 0; i < students.length; i++) {
                    promises.push(students[i]);
                }
                return Promise.all(promises);
            })
            .then((students) => {
                res.send({
                    students: students,
                });
            })
            .catch((reason) => {
                res.send({
                    students: null,
                    error: reason
                });
            });
    }
    else if(surname_to_find == null && student_id_to_find != null)
    {
        console.log("Haku opiskelijanumerolla...");
        // Hakee hakutermit sisältäviä nimiä.
        Student.find({"studentId":{'$regex' : student_id_to_find, '$options' : 'i'}})
            .then((usersFound) => {
                students = usersFound;
                for (let i = 0; i < students.length; i++) {
                    promises.push(students[i]);
                }
                return Promise.all(promises);
            })
            .then((students) => {
                res.send({
                    students: students,
                });
            })
            .catch((reason) => {
                res.send({
                    students: null,
                    error: reason
                });
            });
    }
    else if(surname_to_find != null && student_id_to_find != null)
    {
        console.log("Haku Molemmilla");
        Student.find( { $and: [ { "studentId":{'$regex' : student_id_to_find, '$options' : 'i' } },
            { "surname":{'$regex' : surname_to_find, '$options' : 'i' } } ] } )
            .then((usersFound) => {
                students = usersFound;
                for (let i = 0; i < students.length; i++) {
                    promises.push(students[i]);
                }
                return Promise.all(promises);
            })
            .then((students) => {
                res.send({
                    students: students,
                });
            })
            .catch((reason) => {
                res.send({
                    students: null,
                    error: reason
                });
            });
    }
    else if(surname_to_find == null && student_id_to_find == null)
    {
        console.log("Kumpikin tyhjiä.");
    }
});


router.post('/getStudent', (req, res) => {
    Student.findById(req.body._id)
        .then((item) => {
            res.send({
                item: item,
                message: "Tapahtuma haettu onnistuneesti."
            });
        })
        .catch((reason) => {
            res.send({
                item: null,
                message: `Virhe: ${reason}`
            });
        });
});

router.delete('/deleteStudent/:id', (req, res) => {

    const id = req.params.id;
    Student.findById(id)
        .then((item) => item.remove())
        .then((item) => {
            res.send({
                removedItem: item,
                message: "Item succesfully deleted!"
            });
        })
        .catch((reason) => {
            res.send({
                removedItem: null,
                message: `Error: ${reason}`
            });
        });
});

module.exports = router;