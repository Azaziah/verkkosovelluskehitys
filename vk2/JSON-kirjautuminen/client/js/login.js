'use strict';
var app = angular.module('app', []);

app.controller('Ctrl',['$scope', '$http', function ($scope, $http) {

    $scope.submitLoginForm = function(isValid) {
        // Tarkistetaan formin oikeus.
        if (isValid) {
            // Haetaan varaukseen tulevien tietojen arvot.
            var username = $scope.LoginForm.login_username.$viewValue;
            var password = $scope.LoginForm.login_password.$viewValue;
			$http({
				method: 'POST',
				url: '/login',
				data: {
                    "username": username,
                    "password": password,
                    "lastLoginTime": new Date(),
				}
			})
			.then(function(response) {
                alert("Sisäänkirjautuminen onnistui.");
			}, function(err) {
                alert("Tapahtui virhe!");
			});
        }
        // Jos jokin syötetyistä tiedoista on virheellinen tai puuttuu.
        else {
            // Asetetaan kenttien pristine arvo = false, näin formin puuttuviin/virheellisiin kenttiin tulee virheilmoitus.
            $scope.LoginForm.login_username.$pristine = false;
            $scope.LoginForm.login_password.$pristine = false;
        }
    };

    $scope.submitRegisterForm = function() {
        // Haetaan varaukseen tulevien tietojen arvot.
        var username = $scope.RegisterForm.register_username.$viewValue;
        var password = $scope.RegisterForm.register_password.$viewValue;
        $http({
            method: 'POST',
            url: '/register',
            data: {
                username: username,
                password: password,
            }
        })
            .then(function(response) {
                alert("Käyttäjätunnus: " + username + ' luotiin onnisuneesti.');
            }, function(err) {
                alert("Tunnus on jo olemassa tai käyttäjänimi tai salasana ovat tyhjiä!");
            });
    };
}]);