'use strict';
var app = angular.module('app', []);

// Opiskelijanumeroon voidaan syöttää ainoastaan numeroita.
app.directive('numberInput', function() {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^0-9]*$/, '');
                if(transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

// Estetään numeroiden ja yleisimpien erikoismerkkien syöttö tekstikentissä.
app.directive('textInput', function() {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {

                var transformedInput = text.replace(/[_+!@#¤=?%&£$€{}()\/\\0-9*]/, '');
                if(transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.controller('Ctrl',['$scope', '$http', function ($scope, $http) {

    $scope.fetchAllStudents = function() {

        $scope.student_array = {
            students: []
        };

        $http({
            method: 'GET',
            url: '/getAllStudents'
        })
            .then(function (response) {
                if (response.data.students !== null) {
                    $scope.student_array.students = response.data.students;
                }
            }, function (reason) {
                console.log(reason);
            });
        setTimeout(
            function()
            {
                $scope.addItems();
                // Muutetaan kenttien arvot
                document.getElementById('selected_name').value = "";
                document.getElementById('selected_surname').value = "";
                document.getElementById('selected_calling_name').value = "";
                document.getElementById('selected_student_id').value = "";
                document.getElementById('selected_email').value = "";
            }, 200);
    };

    function Student(name, surname, callingName, studentId, email) {
        this.name = name;
        this.surname = surname;
        this.callingName = callingName;
        this.studentId = studentId;
        this.email = email;
    }

    $scope.addItems = function() {
        var arrayLength = $scope.student_array.students.length;
        const container = document.getElementById('student_list');
        $('#student_list').html($('#student_list').data('old-state'));
        for (let i = 0; i < arrayLength; i++) {
            let id = $scope.student_array.students[i]['_id'];
            let display_name = $scope.student_array.students[i]['callingName'] + ' ' + $scope.student_array.students[i]['surname'];
            container.innerHTML += '' +
                '<div class="result" xmlns="http://www.w3.org/1999/html">' +
                '<div class="col-md-12' + ' block text-center">' +
                '<tr><br><strong class="block-title" style="margin-right: 15px;">' + (display_name) + '</strong>' +
                '<input type="button" value="Valitse" class="btn btn-info" style="float: right; margin-left: 15px;" ' +
                'onClick="getStudentDetails(\'' + id + '\')" />' +
                '<input type="button" value="Poista" style="float: right;"' +
                ' class="btn btn-warning" onClick="deleteStudent(\'' + id + '\')" />' +
                '</tr></div></div>' +
                '</div>';
        }
    };

    $scope.fetchAllStudents();

    deleteStudent = function(id) {
        $http({
            method: 'DELETE',
            url: '/deleteStudent/' + id
        })
            .then(function(response) {
                // Haetaan blokit ja päivitetään ne käyttöliittymään
                setTimeout(
                    function()
                    {
                        $scope.fetchAllStudents();
                    }, 200);
            })
    };

    getStudentDetails = function(id) {

        $http({
            method: 'POST',
            url: '/getStudent',
            data: {
                _id: id
            }
        })
            .then(function(response) {
                if(response.data.item !== null) {
                    const item = response.data.item;
                    $scope.block = angular.copy(item);
                    // Muutetaan kenttien arvot
                    document.getElementById('selected_name').value = response.data.item.name;
                    document.getElementById('selected_surname').value = response.data.item.surname;
                    document.getElementById('selected_calling_name').value = response.data.item.callingName;
                    document.getElementById('selected_student_id').value = response.data.item.studentId;
                    document.getElementById('selected_email').value = response.data.item.email;
                }
            });
    };

    $scope.resetStudentForm = function() {
        // Palautetaan pristine arvot todeksi.
        $scope.studentForm.name.$pristine = true;
        $scope.studentForm.calling_name.$pristine = true;
        $scope.studentForm.surname.$pristine = true;
        $scope.studentForm.student_id.$pristine = true;
        $scope.studentForm.email.$pristine = true;
    };

    $scope.submitStudentForm = function(isValid) {
        // Tarkistetaan formin oikeus.
        if (isValid) {
            $scope.resetStudentForm();
            // Haetaan varaukseen tulevien tietojen arvot.
            var name = $scope.studentForm.name.$viewValue;
            var callingName = $scope.studentForm.calling_name.$viewValue;
            var surname = $scope.studentForm.surname.$viewValue;
            var studentId = $scope.studentForm.student_id.$viewValue;
            var email = $scope.studentForm.email.$viewValue;
            // Tietokantayhteys
			var studentData = {
                "name": name,
                "surname": surname,
                "callingName": callingName,
                "studentId": studentId,
                "email": email
			};
			$http({
				method: 'PUT',
				url: '/addStudent',
				data: {
                    studentData: studentData
				}
			})
			.then(function(response) {
                $scope.fetchAllStudents();
                alert("Opiskelija " + name + " " + surname + " lisättiin onnistuneesti.");
			}, function(err) {
                alert("Tapahtui virhe!");
			});
        }
        // Jos jokin syötetyistä tiedoista on virheellinen tai puuttuu.
        else {
            // Asetetaan kenttien pristine arvo = false, näin formin puuttuviin/virheellisiin kenttiin tulee virheilmoitus.
            $scope.studentForm.name.$pristine = false;
            $scope.studentForm.calling_name.$pristine = false;
            $scope.studentForm.surname.$pristine = false;
            $scope.studentForm.student_id.$pristine = false;
            $scope.studentForm.email.$pristine = false;
        }
    };

    $scope.submitSearchForm = function() {
    // check to make sure the form is completely valid
        // Haetaan varaukseen tulevien tietojen arvot.
        const surname_to_find = $scope.searchForm.search_surname.$viewValue;
        const student_id_to_find = $scope.searchForm.search_student_id.$viewValue;

        console.log(surname_to_find);
        $scope.student_array = {
            students: []
        };

        // Tietokantayhteys
        var searchData = {
            surname: surname_to_find,
            studentId: student_id_to_find,
        };
        $http({
            method: 'POST',
            url: '/getSearchedStudents',
            data: {
                "surname": surname_to_find,
                "studentId" : student_id_to_find,
            }
        })
            .then(function (response) {
                if (response.data.students !== null) {
                    $scope.student_array.students = response.data.students;
                }
            }, function (reason) {
                console.log(reason);
            });
        setTimeout(
            function()
            {
                $scope.addItems();
                // Muutetaan kenttien arvot
                document.getElementById('selected_name').value = "";
                document.getElementById('selected_surname').value = "";
                document.getElementById('selected_calling_name').value = "";
                document.getElementById('selected_student_id').value = "";
                document.getElementById('selected_email').value = "";
            }, 200);
    };
}]);