/******** MIT-lisenssi: ks. License.txt ********/

/* Moduulit */

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

/* Skeemat */
var User = require('./schemas/user_schema.js');
var Student = require('./schemas/student_schema.js');

module.exports = {

    init: function() {
        /* Yhdistetään tietokantaan. Se luodaan, ellei sitä ole. */
        mongoose.connect('mongodb://localhost/data/db:27017');
    }
};
