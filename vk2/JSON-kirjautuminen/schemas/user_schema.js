'use strict';

const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    username: { type: String, required: true, index: { unique: false } },
    password: { type: String, required: true, index: { unique: false } },
    lastLoginTime: { type: Date, required: false, index: { unique: false } }
});

module.exports = mongoose.model('User', UserSchema);