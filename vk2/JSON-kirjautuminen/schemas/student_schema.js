'use strict';

const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var StudentSchema = new Schema({
    name: { type: String, required: true, index: { unique: false } },
    surname: { type: String, required: true, index: { unique: false } },
    callingName: { type: String, required: true, index: { unique: false } },
    studentId: { type: String, required: true, index: { unique: false } },
    email: { type: String, required: true, index: { unique: false } }
});

module.exports = mongoose.model('Student', StudentSchema);