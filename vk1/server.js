const express = require('express');

var app = express();
var Student = require('./student.js');

const etunimi = ["Olli", "Matti", "Pate"];
const sukunimi = ["Suutari", "Mahti", "Patelli"];

app.get('/get_student', function (req, res) {
    //
    var student = new Student("Matti Petteri", "Mahti", "Matti", "1993", "00000", "123456@edu.karelia.fi");
    student.set_studentId(123456);
    // Arvotaan listoista nimet (tunnilla läpikäyty koodi)
    var etu = etunimi[Math.floor(Math.random()*etunimi.length)];
    var suku = sukunimi[Math.floor(Math.random()*sukunimi.length)];
    // Tulostetaan random etunimi + sukunimi sekä kutsutaan student.introduce().
    res.send(etu + " " + suku + "<br><br>" + student.introduce());
});

app.get('/', function (req, res) {
    res.send("Hakemisto on tyhjä.");
});

const PORT = 8000;
const HOST = '0.0.0.0';

app.listen(PORT, HOST, function () {
   console.log("\nKuunnellaan osoitetta http://" + HOST + ":" + PORT)
});