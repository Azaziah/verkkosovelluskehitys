### Olli Suutari | Verkkosovelluskehitys vk 1


### Viikon 1 raportti

Tällä viikolla harjoittelimme yksinkertaisen REST reitityksen tekemistä.


Sain ymmärtääkseni tehtyä tehtävän vaatimusten mukaisesti.
Tehtävän tekeminen ei tuottanut erityisesti hankaluuksia, tähän vaikutti toki myös aiempi kokemus aiheesta.

Viikon 1 tiedostoja pääsee tarkastelemaan [tämän linkin kautta](https://gitlab.com/Azaziah/verkkosovelluskehitys-olli-suutari/tree/master/vk1), tai klikkamalla polkua Gitlabissa.